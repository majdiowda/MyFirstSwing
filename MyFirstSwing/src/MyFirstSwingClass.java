import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class MyFirstSwingClass extends JFrame{

	private JTextField textInputField;
	private JLabel textInputLabel;
	private JLabel hashValueLabel;
	
	public MyFirstSwingClass( )
	{
	
		 this.setTitle( "Name Frame" );
		 Container contentPane = this.getContentPane();
		 contentPane.setLayout( new GridLayout(4,2) );

		 contentPane.add( new JLabel( "  Enter text to hash "  ) );
		 textInputField = new JTextField( );
		 contentPane.add( textInputField );

		 contentPane.add( new JLabel( "  You have entered: " ) );
		 textInputLabel = new JLabel("??" ) ;
		 contentPane.add( textInputLabel );

		 contentPane.add( new JLabel( "  The hash is: " ) );
		 hashValueLabel = new JLabel( "??") ;
		 contentPane.add( hashValueLabel  );
		 
		 TextInputListener inputListener = new TextInputListener( );
		 textInputField.addActionListener( inputListener );
		 
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MyFirstSwingClass theWindow = new MyFirstSwingClass( );

		theWindow.setSize( 800, 400 );
		 theWindow.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		 theWindow.setVisible(true);		
	}

	private class TextInputListener implements ActionListener
	{
	 public void actionPerformed( ActionEvent e )
	 {
		 textInputLabel.setText( textInputField.getText() );
		 hashValueLabel.setText( " HASH TO APPEAR HERE !! " );
	 }
	}
	
}
